package com.target.gennotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationStarter {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(NotificationStarter.class, args);
	}
}
