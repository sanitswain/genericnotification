package com.target.gennotification.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.target.gennotification.channels.NotificationStrategy;
import com.target.gennotification.vo.Notification;

/**
 * @author Sanit
 * 
 *         <p>
 *         Service class to find an appropriate notification strategy
 *         implementor to handle request
 * 
 *         Future enhancements:
 *         <ul>
 *         <li>Inclusion of securities and authorization</li>
 *         <li>Handling multiple receivers</li>
 *         </ul>
 *
 */
@Component
public class NotificationService {

	@Autowired
	private List<NotificationStrategy> strategies;

	public void send(Notification notification) throws Exception {
		for (NotificationStrategy strategy : strategies) {
			if (strategy.canNotify(notification)) {
				strategy.notify(notification);
				break;
			}
		}
	}
}
