package com.target.gennotification.channels;

import com.target.gennotification.vo.Notification;

public interface NotificationStrategy {

	/**
	 * Defines if current strategy implementor can handle request
	 */
	public boolean canNotify(Notification notification);

	/**
	 * Handles notification request
	 */
	public void notify(Notification notification) throws Exception;
}
