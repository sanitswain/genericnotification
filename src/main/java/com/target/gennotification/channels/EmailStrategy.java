package com.target.gennotification.channels;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.target.gennotification.vo.Notification;

@Configuration
public class EmailStrategy implements NotificationStrategy {

	@Resource(name = "emailConfig")
	private Properties emailConfig;

	@Resource(name = "emailBasics")
	private Map<String, String> emailBasics;

	@Override
	public boolean canNotify(Notification notification) {
		return notification.getChannel().equalsIgnoreCase("email");
	}

	@Override
	public void notify(Notification notification) throws Exception {
		System.out.println("Email :: following notification sent\n" + notification);
		sendMail(notification.getReceiver(), notification.getMessage());
	}

	public void sendMail(String to, String msg) {
		// get Session
		Session session = Session.getDefaultInstance(emailConfig, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailBasics.get("username"), emailBasics.get("password"));
			}
		});

		// compose message
		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject(emailBasics.get("subject"));
			message.setText(msg);
			// send message
			Transport.send(message);
			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean("emailConfig")
	@ConfigurationProperties(prefix = "email_config")
	public Properties properties() {
		return new Properties();
	}

	@Bean("emailBasics")
	@ConfigurationProperties(prefix = "email_basic")
	public Map<String, String> emailBasicDetails() {
		return new HashMap<>();
	}

}
