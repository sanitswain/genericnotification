package com.target.gennotification.channels;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.target.gennotification.vo.Notification;

/**
 * @author Sanit
 * 
 *         <p>
 *         Converts message to slack convenient json message and sends to
 *         configured incoming web hook
 *
 */
@Configuration
public class SlackStrategy implements NotificationStrategy {

	@Value("${slack.web_hook}")
	private String hookUrl;

	@Resource(name = "slackClient")
	private RestTemplate template;

	@Override
	public boolean canNotify(Notification notification) {
		return notification.getChannel().equalsIgnoreCase("slack");
	}

	@Override
	public void notify(Notification notification) throws Exception {
		System.out.println("Slack :: following notification sent\n" + notification);
		ResponseEntity<String> response = template.postForEntity(hookUrl, convertToSlackMsg(notification),
				String.class);
		if (response.getStatusCode() != HttpStatus.OK) {
			throw new RuntimeException(response.getBody());
		}
	}

	/**
	 * Converts to slack convenient json message
	 */
	private Map<String, Object> convertToSlackMsg(Notification notification) {
		Map<String, Object> map = new HashMap<>();
		String text = String.format("@%s- %s - By %s", notification.getReceiver(), notification.getMessage(),
				notification.getSender());
		map.put("text", text);
		return map;
	}

	@Bean("slackClient")
	public RestTemplate slackClient() {
		RestTemplate template = new RestTemplate();
		return template;
	}

}
