package com.target.gennotification.channels;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.target.gennotification.vo.Notification;

/**
 * @author Sanit
 * 
 *         <p>
 *         Fall-back strategy class to throw exception when no appropriate
 *         implementor found. It's priority configured to lower value such that
 *         it will at as last option to handle notification request
 *
 */
@Order(Integer.MAX_VALUE)
@Component
public class DefaultStrategy implements NotificationStrategy {

	@Override
	public boolean canNotify(Notification notification) {
		return true;
	}

	@Override
	public void notify(Notification notification) throws Exception {
		throw new Exception("Didn't find suitable notification strategy");
	}

}
