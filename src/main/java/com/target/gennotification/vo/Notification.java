package com.target.gennotification.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Notification {

	private String sender;
	private String receiver;
	private String message;

	@JsonIgnore
	private String channel;

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return "Notification [sender=" + sender + ", receiver=" + receiver + ", message=" + message + "]";
	}

}
