package com.target.gennotification.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.target.gennotification.service.NotificationService;
import com.target.gennotification.vo.Notification;

@RestController
public class NotificationController {

	@Autowired
	private NotificationService service;

	@RequestMapping(value = "/notify/{channel}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<String, Object> notify(@RequestBody Notification notification, @PathVariable("channel") String channel) {
		Map<String, Object> response = new LinkedHashMap<>();
		try {
			notification.setChannel(channel);
			service.send(notification);

			response.put("status", "success");
			// TODO: Implement acknowledge flow
			response.put("message", "Message sent");

		} catch (Exception e) {
			response.put("status", "failed");
			response.put("reason", e.getMessage());
		}

		return response;
	}
}
