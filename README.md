## Application set up

_Update application.properties file with following changes_

1. Update `slack.web_hook` key with our slack incoming web hook url
2. Update `email_basic.username` and `email_basic.password` keys with your Gmail credential


_Testing steps_

1. For **slack** use `http://localhost:8081/notify/slack` and **email** use `http://localhost:8081/notify/email` endpoints
2. Method: `POST`
3. Headers: `Content-Type: application/json`
4. Request payload. Payload is same for both types.

>
	  {
	  	"sender": "sanit",
	  	"receiver": "sanitswain2008@gmail.com",
	  	"message": "Hello user"
	  }
